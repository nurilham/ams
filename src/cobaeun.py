import csv
import re
import urllib2
import sys
from senti_classifier import senti_classifier
reload(sys)
sys.setdefaultencoding('utf8')

""" Translasi tweet dengan web-scrapping """
def translate(to_translate, to_langage="auto", langage="auto"):
	'''Return the translation using google translate
	you must shortcut the langage you define (French = fr, English = en, Spanish = es, etc...)
	if you don't define anything it will detect it or use english by default
	Example:
	print(translate("salut tu vas bien?", "en"))
	hello you alright?'''
	agents = {'User-Agent':"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)"}
	before_trans = 'class="t0">'
	link = "http://translate.google.com/m?hl=%s&sl=%s&q=%s" % (to_langage, langage, to_translate.replace(" ", "+"))
	request = urllib2.Request(link, headers=agents)
	page = urllib2.urlopen(request).read()
	result = page[page.find(before_trans)+len(before_trans):]
	result = result.split("<")[0]
	return result

""" Fungsi untuk menghilangkan yg tidak perlu dari tweet (preprocessing) """
def preprocess(teks):
	list_kata = teks.split()
	
	"""output fungsi hasil preprocess"""
	preprocessed_teks = list()
	
	"""persiapan stopword indonesia"""
	filestopword = open("stopwordsindo.txt")
	teksstopword = filestopword.read()
	stopwords = teksstopword.split()
	
	"""iterasi tiap kata di tweet, dibuang yg ga perlu"""
	for token in list_kata:
		
		token = token.lower()						
		if token[:1] == '@':
			preprocessed_teks.append(token)
			continue
		elif token[:1] == '#' :
			preprocessed_teks.append(token)
			continue
		elif token[:4] == 'http':
			"""buang link"""
			continue
		elif token in stopwords:
			"""buang stopwords"""
			continue
		else:
			"""buang symbol"""
			token = re.sub(r'[^\w]', ' ', token)	
			preprocessed_teks.append(token)
			
	filestopword.close()
	return ' '.join(preprocessed_teks)

if __name__ == "__main__":
	
	""" Persiapan output txt file  """
	fileoutput = open("fileoutput.txt","w+")
	
	""" Persiapan file input csv"""
	csv.register_dialect('titikkoma',delimiter=';')
	csvfile = open("uber.csv","rb")
	csvreader = csv.reader(csvfile, dialect='titikkoma')
	
	""" Persiapan file output csv"""
	csvfile_output = open("uber_output.csv","wt")
	csvwriter = csv.writer(csvfile_output, dialect='titikkoma')
	csvwriter.writerow(('Username','Translated Tweet','Positive Score','Negative Score','Class'))
	
	""" Iterasi tiap row di file .csv-nya """
	rownum = 0
	for row in csvreader:
		"""nge-save header"""
		if rownum == 0:
			header = row
		else :
			"""nge-print kolom tweet-nya (row[2]) saja"""
			fileoutput.write("tweet : \n")
			fileoutput.write(row[2] + "\n")
			
			"""nge-print hasil preprocessing tweet"""
			preprocessed_tweet = preprocess(row[2])
			fileoutput.write("preprocessed :\n")
			fileoutput.write(preprocessed_tweet + "\n")
			
			"""nge-print hasil translasi tweet"""
			translated_tweet = translate(preprocessed_tweet)
			fileoutput.write("translated :\n")
			fileoutput.write(translated_tweet + "\n")
			
			"""nge-print nilai hasil klasifikasi sentiword """
			try :
				polarity_tweet = [translated_tweet]
				pos_score, neg_score = senti_classifier.polarity_scores(polarity_tweet)
				fileoutput.write("positive score : " + str(pos_score) + "  ")
				fileoutput.write("negative score : " + str(neg_score) + "\n")
				if(pos_score > neg_score):
					csvwriter.writerow((row[1],translated_tweet, pos_score, neg_score, "POSITIVE"))
				elif(pos_score < neg_score):
					csvwriter.writerow((row[1],translated_tweet, pos_score, neg_score, "NEGATIVE"))
				else:
					csvwriter.writerow((row[1],translated_tweet, pos_score, neg_score, "NEUTRAL"))
				
			except UnicodeDecodeError:
				fileoutput.write("positive score : error  ")
				fileoutput.write("negative score : error\n")
				csvwriter.writerow((row[1],translated_tweet, 'error', 'error','UNIDENTIFIED'))
		
		rownum += 1
		print str(rownum) + " done!"
		fileoutput.write("\n")
		
		"""nge-batesin tweet hanya n tweet pertama"""
		if rownum-1 == 20:
			break
	
	""" closing """
	fileoutput.close()
	csvfile.close()
	csvfile_output.close()
